package com.prepflash.engine.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import com.prepflash.engine.documentProcessing.MainContentExtractor;
import com.prepflash.engine.documentProcessing.PDFBoxAdapter;

public class TestTextExtract {
	
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	static void testXML() {
		try {
			URL url = new URL("https://trello-attachments.s3.amazonaws.com/558b6432d0e4b04875b93b2a/560a00dff458274513dcf659/ee195ddac3d8b0b0cf2b44b8f05cc930/test.xml");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");

			String xml = convertStreamToString(conn.getInputStream());
			InputStream is = new ByteArrayInputStream( xml.getBytes( StandardCharsets.UTF_8 ) );
			
			MainContentExtractor ex = new MainContentExtractor();
			ex.process( is );
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void testPDFtoText() {
        File file = new File("/Users/liao/Downloads/RAD100 Radiant Fundamentals.pdf");
        PDFBoxAdapter pdf = new PDFBoxAdapter();
        try {
			String text = pdf.parse(new FileInputStream(file));
			System.out.println(text);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
	
	public static void main(String[] args) {
		testPDFtoText();
	}

}
