package com.prepflash.engine.test;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prepflash.engine.analysis.FeatureExtractor;
import com.prepflash.engine.analysis.SparseVector;
import com.prepflash.engine.generator.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Main {

	static String inputFile = "input.txt";
	
	private static String getStringFromInputStream(InputStream is, Charset encoding) {
		 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is, encoding));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
	}
	
	static String readFile(String filename, Charset encoding) throws IOException, URISyntaxException 
	{
//		String jar_path = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
//		String input_path = jar_path + "/" + filename;
		InputStream in = Main.class.getClassLoader().getResourceAsStream("com/prepflash/engine/test/" + filename);
		return getStringFromInputStream(in, encoding);
//		byte[] encoded = Files.readAllBytes(input_path);
//		return new String(encoded, encoding);
	}
	
	static String wikiContent( String wikiURL ) 
	{
		try {
			Document doc = Jsoup.connect(wikiURL).get();
			Elements nodes = doc.select("p");
			String str = "";
			for( Element node : nodes ) {
				str += node.text() + "\n";
			}
			return str;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	public static void main(String[] args) throws IOException, URISyntaxException {	
		// String wikiURL = "http://www.cnn.com/2015/07/21/politics/republican-two-weeks-debates-preview/index.html";
		// String wikiURL = "https://en.wikipedia.org/wiki/Crystal";
		// String wikiURL = "https://en.wikipedia.org/wiki/Protein";
		// String wikiURL = "https://en.wikipedia.org/wiki/Key_West";
		String wikiURL = "https://en.wikipedia.org/wiki/Taiwan";
		
		System.out.println("START");
//		String article = readFile( inputFile, StandardCharsets.UTF_8 );
		String article = wikiContent( wikiURL );
		
		SimpleEngine gen = new SimpleEngine();
		SimpleEngine.Option option = new SimpleEngine.Option();
		option.keygenUsePrepFlash = true;
		option.keygenUseSmile = false;
		option.keygenUseAlchemy = false;
		option.keygenUseWiki = false;
		option.keygenUseNER = false;
		
		System.out.println("GO!");
		for( int i = 0; i < 1; ++i ) {
			long t0 = System.currentTimeMillis();
			SimpleEngine.Result results = gen.createCards(article, "wiki", option);
			Object[] keywords = 
					results.sentences.stream()
							 		 .flatMap((x) -> x.getKeywords().stream())
							 		 .parallel()
							 		 .reduce(new HashSet<String>(), 
							 				 (c, x) -> {
							 					c.add(x.getText());
							 					return c;
							 				 }, (c1, c2) -> {
							 					c1.addAll(c2);
												return c1;
							 				 })
							 		 .toArray();
			long t1 = System.currentTimeMillis();
			System.out.println((t1-t0)/1000.0 + "\n");
			
			if (i == 0 ) {
				Gson gson = new GsonBuilder().disableHtmlEscaping().create();
				Map<String, Object> json = new HashMap<String, Object>();
				json.put( "data", results.cards );
				json.put( "keyword", keywords);
				System.out.println(gson.toJson(json));
			}
		}
	}
	
//	// test NER
//	public static void main(String[] args) throws IOException, URISyntaxException {
//		String article = "George Washington has $200,423,512 USD and 3.4% higher than 1/4 Georgia and United Nation in October 1st, 1947, 5:45am. \n";
//		article += article;
//		article += article;
//		
//		article = "As of 2015, Microsoft is market dominant in both the IBM PC-compatible operating system (while it lost the majority of the overall operating system market to Android) and office software suite markets (the latter with Microsoft Office). The company also produces a wide range of other software for desktops and servers, and is active in areas including Internet search (with Bing), the video game industry (with the Xbox, Xbox 360 and Xbox One consoles), the digital services market (through MSN), and mobile phones (via the operating systems of Nokia's former phones[12] and Windows Phone OS). In June 2012, Microsoft entered the personal computer production market for the first time, with the launch of the Microsoft Surface, a line of tablet computers.";
//		
//		SimpleEngine eng = new SimpleEngine();
//		SimpleEngine.Result res = eng.createCards(article, "text");
//		for(Card c : res.cards ) {
//			System.out.println(c.getType());
//			System.out.println(c.getQuestion());
//			System.out.println(c.getAnswer());
//			System.out.println();
//		}
//	}
	
//	// test feature vector
//	public static void main(String[] args) throws IOException, URISyntaxException {
//		String text_1 = readFile( "protein.txt", StandardCharsets.UTF_8 );
//		String text_2 = readFile( "bathymetry.txt", StandardCharsets.UTF_8 );
////		String text_3 = readFile( "keywest.txt", StandardCharsets.UTF_8 );
//		
//		String text_3 = wikiContent( "https://en.wikipedia.org/wiki/Protein_biosynthesis" );
//		
//		FeatureExtractor fe = new FeatureExtractor();
//		
//		System.out.println( "Starting analysis..." );
//		long t1 = System.currentTimeMillis();
//		
//		SparseVector<Double> fv1 = fe.computeFeatureVector( text_1 );
//		SparseVector<Double> fv2 = fe.computeFeatureVector( text_2 );
//		SparseVector<Double> fv3 = fe.computeFeatureVector( text_3 );
//		
//		long t2 = System.currentTimeMillis();
//		System.out.println( "Analysis Time = " + (double)(t2-t1) / 1000f + " seconds." );
//		
//		System.out.println( fv1.numberOfNonZeroEntry() );
//		System.out.println( fv2.numberOfNonZeroEntry() );
//		System.out.println( fv3.numberOfNonZeroEntry() );
//		String str1 = fv1.toString((x) -> Integer.toHexString( Float.floatToIntBits( x.floatValue() ) ));
//		String str2 = fv1.toString();
//		System.out.println(str1);
//		System.out.println();
//		System.out.println(str2);
//		
//		System.out.println( "1 vs 1 = " + fe.cosine_similarity(fv1, fv1));
//		System.out.println( "1 vs 2 = " + fe.cosine_similarity(fv1, fv2));
//		System.out.println( "1 vs 3 = " + fe.cosine_similarity(fv1, fv3));
//		System.out.println();
//
//		System.out.println( "2 vs 1 = " + fe.cosine_similarity(fv2, fv1));
//		System.out.println( "2 vs 2 = " + fe.cosine_similarity(fv2, fv2));
//		System.out.println( "2 vs 3 = " + fe.cosine_similarity(fv2, fv3));
//		System.out.println();
//
//		System.out.println( "3 vs 1 = " + fe.cosine_similarity(fv3, fv1));
//		System.out.println( "3 vs 2 = " + fe.cosine_similarity(fv3, fv2));
//		System.out.println( "3 vs 3 = " + fe.cosine_similarity(fv3, fv3));
//		System.out.println();
//
//	}
}
