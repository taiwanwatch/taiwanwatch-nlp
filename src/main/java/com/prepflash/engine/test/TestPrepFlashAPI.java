package com.prepflash.engine.test;

import org.json.JSONArray;
import org.json.JSONObject;

import com.prepflash.engine.analysis.FeatureExtractor;
import com.prepflash.engine.analysis.SparseVector;
import com.prepflash.engine.util.PrepFlashAPI;

public class TestPrepFlashAPI {

	public static String convertCardsToText( JSONArray json ) {
		String str = "";
		for( int i = 0; i < json.length(); ++i ) {
			JSONObject card = json.getJSONObject(i);
			/*
			String ques = card.getString("question");
			String ansr = card.getString("answer");
			String sent = ques.replace("________", ansr );
			str += sent + " ";
			*/
			
			JSONArray keys = card.getJSONArray("keyword");
			for( int j = 0; j < keys.length(); ++j ) {
				str += keys.getString(j) + " ";
			}
		}
		return str;
	}
	
	public static void main(String[] args) {
		//String set_id = "0b3ac320-7551-11e6-8d9d-2d7a8cdf129e";
		JSONArray set1 = PrepFlashAPI.setCards( "eb9d4e80-754f-11e6-aa58-c7d40f29a01f" ); // X-ray (NER)
		JSONArray set2 = PrepFlashAPI.setCards( "e853aa10-7876-11e6-88eb-1dac4ec73f77" ); // Stanford university
		JSONArray set3 = PrepFlashAPI.setCards( "dcf72360-4a4d-11e6-ba85-69b03b24b598" ); // X-ray
		JSONArray set4 = PrepFlashAPI.setCards( "757b9900-011a-11e5-bcfc-9dcf0884b953" ); // Crystal
		JSONArray set5 = PrepFlashAPI.setCards( "1ba12180-7535-11e6-aa58-c7d40f29a01f" ); // Taiwan (NER)
				
		System.out.println( set1.toString(2) );
		
		FeatureExtractor fe = new FeatureExtractor();
		SparseVector<Double> fv1 = fe.computeFeatureVector( convertCardsToText( set1 ) );
		SparseVector<Double> fv2 = fe.computeFeatureVector( convertCardsToText( set2 ) );
		SparseVector<Double> fv3 = fe.computeFeatureVector( convertCardsToText( set3 ) );
		SparseVector<Double> fv4 = fe.computeFeatureVector( convertCardsToText( set4 ) );
		SparseVector<Double> fv5 = fe.computeFeatureVector( convertCardsToText( set5 ) );

		System.out.println( fv1.numberOfNonZeroEntry() );
		System.out.println( fv2.numberOfNonZeroEntry() );
		System.out.println( fv3.numberOfNonZeroEntry() );
		System.out.println( fv4.numberOfNonZeroEntry() );
		System.out.println( fv5.numberOfNonZeroEntry() );
		
		System.out.println( fe.cosine_similarity(fv1, fv2) );
		System.out.println( fe.cosine_similarity(fv1, fv3) );
		System.out.println( fe.cosine_similarity(fv1, fv4) );
		System.out.println( fe.cosine_similarity(fv1, fv5) );

		//System.out.println( fv1.toString().length() );
		//System.out.println( fv1.toString((x) -> Integer.toHexString( Float.floatToIntBits( x.floatValue() ) )).length() );
	}

}
