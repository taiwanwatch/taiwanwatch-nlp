package com.prepflash.engine.generator;

import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.Tree;

public class Keyword implements Comparable<Keyword> {

	private String text;
	private int numOccr = 0;
	private ArrayList<Keyword> assctkeys = new ArrayList<Keyword>();
	private List<String> lemma;
	private Tree tree;
	private List<TaggedWord> taggedWord;
	private boolean isPlural; 
	
	public Keyword( String keyword ) {
		this.text = keyword;
	}
	
	public String getText() {
		return this.text;
	}
		
	public boolean isPlural() {
		return this.isPlural;
	}
	
	public String getTextPlural() {
		return this.isPlural() ? this.getText() : English.plural(this.getText());
	}
	
	public String getTextSingular() {
		if (!this.isPlural()) {
			return this.getText();
		} else {
			String s = "";
			for( String x : this.lemma ) {
				s += x + " ";
			}
			return s.trim();
		}
	}
	
	public void setLemma( List<String> lem ) {
		this.lemma = lem;
		this.updatePlural();
	}

	public List<String> getLemma() {
		return this.lemma;
	}

	public void setTree( Tree p ) {
		this.tree = p;
	}
	
	public Tree getTree() {
		return this.tree;
	}

	public void setTaggedWords( List<TaggedWord> p ) {
		this.taggedWord = p;
		this.updatePlural();
	}

	public List<TaggedWord> getTaggedWords() {
		return this.taggedWord;
	}
	
	public void setNumberOfOccurrence( int occr ) {
		this.numOccr = occr;
	}
	
	public int getNumberOfOccurrence() {
		return this.numOccr;
	}

	public double getNumberOfOccurrenceInverse() {
		return (this.numOccr == 0) ? 0.0 : 1.0 / (double)this.numOccr;
	}
	
	public void setAssociatedKeywords( List<Keyword> keys ) {
		this.assctkeys.clear();
		this.assctkeys.addAll(keys);
	}
	
	public List<Keyword> getAssociatedKeywords() {
		return this.assctkeys;
	}
	
	@Override
	public int compareTo(Keyword o) {
		return Integer.compare(this.numOccr, o.numOccr);
	}

	@Override
    public boolean equals(Object x) {
		if (this == x) {
			return true;
		} else if( x instanceof Keyword ) {
			return this.text.equals( ((Keyword)x).getText() );	
		}
		return false;
		
	}
	
	@Override
	public String toString() {
		return this.text;
	}

	private void updatePlural() {
		this.isPlural = false;
		if( this.lemma != null && this.taggedWord != null ) {
			if( this.lemma.size() == this.taggedWord.size() ) {
				boolean same = true;
				for( int i = 0; i < this.lemma.size(); ++i ) {
					same &= this.lemma.get(i).toUpperCase().equals( this.taggedWord.get(i).value().toUpperCase() );
				}
				this.isPlural = !same;
			}
		}
	}

}
