package com.prepflash.engine.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.prepflash.engine.common.Configuration;

import edu.stanford.nlp.util.Triple;

public abstract class Card 
{
	protected String question = "";
	protected String answer = "";
	protected String previous  = "";
	protected String source = "";
	protected String type = "";
	protected float difficulty = 0;
	protected List<String> keyword = new ArrayList<String>();
	protected String note = "";
	protected Integer sentenceId = null;
	
	public String getQuestion() { 
		return this.question; 
	}
	
	public String getAnswer() {
		return this.answer;
	}
		
	public String getNote() {
		return this.note;
	}
	
	public abstract String getType();
	
	protected Card( Sentence s ) {		
		// keyword
		this.keyword = s.getKeywords().stream().map((x) -> x.getText()).collect(Collectors.toList());
		// source
		String source = s.getSource();
		if (source != null) {
			this.source = source;
		}
		// prev. sent.
		String prev = s.getPreviousSentence();
		if (prev != null) {
			this.previous = prev; 
		}
		// difficulty
		this.difficulty = (float)s.getScore(); // don't need so nigh precision for clients
		// type
		this.type = this.getType();
		// annotation
		if (s.getAnnotations().size() > 0) {
			this.note = s.getAnnotations().toString();
		}
		// sentence Id
		this.sentenceId = s.getId();
	}
}

class CardFillInBlank extends Card 
{
	public CardFillInBlank( Sentence s ) {
		super(s);
//		if( this.getType().equals("blank") && s.getNERSpans().size() > 0 ) {
//			// NER Cards
//			this.difficulty = 1.0f;
//			List<Triple<String, Integer, Integer>> ners = s.getNERSpans();
//			int rand_ner = (int)( Math.random() * ners.size() );
//			int span_first  = ners.get(rand_ner).second();
//			int span_second = ners.get(rand_ner).third();
//			String str = s.getText();
//			this.question = str.substring(0, span_first) + "________" + str.substring( span_second, str.length() );
//			this.answer = str.substring(span_first, span_second);
//		} else {
			// normal Card
			String str = s.getText();
			Sentence.QuestionDefine ques = s.getQuestionDefine();
			this.question = str.substring(0, ques.span.first) + "________" + str.substring( ques.span.second, str.length() );
			this.answer = str.substring(ques.span.first, ques.span.second);
//		}
	}

	@Override
	public String getType() {
		return "blank";
	}

	@Override
	public String toString() {
		return this.getType() + "\n\n" + this.question + "\n\n" + "ANS: " + this.answer;
	}
}

class CardMultiChoice extends CardFillInBlank 
{
	protected List<String> choice = new ArrayList<String>();

	public CardMultiChoice( Sentence s ) {
		super(s);
		Sentence.QuestionDefine ques = s.getQuestionDefine();
		this.choice.addAll( ques.decoys );
		
		// insert correct answer
		String correct = s.getText().substring( ques.span.first, ques.span.second );
		this.choice.set((int)(Math.random()*Configuration.SETTINGS.NUM_CHOICE), correct );
	}
	
	public List<String> getDecoys() { 
		return this.choice;
	}

	@Override
	public String getType() {
		return "multiple";
	}

	@Override
	public String toString() {
		return this.getType() + " \n\n" +  this.question + "\n\n" + "ANS: " + this.answer + "\n\n" + "DECOYS: " + this.choice;
	}	
}

class CardTrueFalse extends Card
{
	protected String decoy = "";
	protected int decoy_begin = -1;
	
	public CardTrueFalse( Sentence s ) {
		super(s);
		
		Sentence.QuestionDefine ques = s.getQuestionDefine();
		
		boolean is_true = Math.random() > 0.5;
		if (is_true) { // True
			this.question = s.getText();
			this.answer = "True";
			this.decoy = s.getText().substring(ques.span.first, ques.span.second);
			this.decoy_begin = ques.span.first;
		} else { // False
			String str = s.getText();
			String decoy = ques.decoys.get( (int)(Math.random()*ques.decoys.size()) );
			String correct = str.substring(ques.span.first, ques.span.second);
			this.question = str.substring(0, ques.span.first) + decoy + str.substring( ques.span.second, str.length() );
			this.answer = "False (" + decoy + " => " + correct + ")";
			this.decoy = decoy;
			this.decoy_begin = ques.span.first;
		}
	}
	
	@Override
	public String getType() {
		return "true/false";
	}

	@Override
	public String toString() {
		return this.getType() + "\n\n" + this.question + "\n\n" + "ANS: " + this.answer;
	}
}