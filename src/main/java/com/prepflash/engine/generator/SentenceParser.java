package com.prepflash.engine.generator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.prepflash.engine.common.Configuration;

import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.Parser;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;

public class SentenceParser {
	
	private Parser parser = null;

	public static ParserModel createModel() {
		try {
			InputStream modelIn = new FileInputStream(Configuration.SETTINGS_PCKG.OPENNLP_MODEL_PARSE);
			ParserModel model = new ParserModel(modelIn);
			return model;
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		return null;
	}

	public SentenceParser() {
		this.parser = ParserFactory.create( createModel() );
	}
	
	public SentenceParser( ParserModel m ) {
		this.parser = ParserFactory.create( m );
	}
	
	public Parse [] process( String str ) {
		return ParserTool.parseLine(str, this.parser, 1); 
	}
}
