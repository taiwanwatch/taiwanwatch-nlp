package com.prepflash.engine.generator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.prepflash.engine.analysis.EnglishDefaultStopWord;
import com.prepflash.engine.analysis.EnglishLongStopWord;
import com.prepflash.engine.analysis.GoogleHistoryStopWord;
import com.prepflash.engine.analysis.MySQLStopWord;
import com.prepflash.engine.analysis.StopWordChecker;
import com.prepflash.engine.common.Configuration;
import com.prepflash.engine.common.Configuration.SETTINGS;
import com.prepflash.engine.generator.Sentence.QuestionDefine;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.shiftreduce.ShiftReduceParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.util.Pair;
import edu.stanford.nlp.util.Triple;
import smile.nlp.NGram;
import smile.nlp.keyword.*;

public class SimpleEngine {
	
	static public class Result {
		public List<Card> cards;
		public List<Sentence> sentences;
		
		public Result( List<Card> cards, List<Sentence> sents ) {
			this.cards = cards;
			this.sentences = sents;
		}
	}

	static public class KeywordResult {
		public String keyword;
		public Integer occurs;
		
		public KeywordResult( Keyword key ) {
			this.keyword = key.getText();
			this.occurs = key.getNumberOfOccurrence();
		}
	}

	static public class Option {
		public int debug = 0;
		public boolean skipSentencePreprocessing = false;
		public boolean keygenUsePrepFlash = true;
		public boolean keygenUseAlchemy = true;
		public boolean keygenUseSmile = true;
		public boolean keygenUseWiki = true;
		public boolean keygenUseNER = true;
		public int keygenOptionSmileMaxKeyNum = -1;

		public boolean useDebug() {
			return debug > 0;
		}
	}
	
	private ExecutorService workers = Executors.newCachedThreadPool();

    private LexicalizedParser lp = LexicalizedParser.loadModel(Configuration.SETTINGS_PCKG.STANFORD_NLP_GRAMMAR);
    private TreebankLanguagePack tlp = lp.getOp().langpack();
    private MaxentTagger tagger = new MaxentTagger(Configuration.SETTINGS_PCKG.STANFORD_NLP_TAGGER_PATH);
    private ShiftReduceParser model = ShiftReduceParser.loadModel(Configuration.SETTINGS_PCKG.STANFORD_NLP_MODEL_PATH);
    private StanfordSentenceAnnotator lemma = new StanfordSentenceAnnotator();
    private CooccurrenceKeywordExtractor smileKeywordGen = new CooccurrenceKeywordExtractor();
    private StopWordChecker swchk = new StopWordChecker( new EnglishDefaultStopWord(), new EnglishLongStopWord(), new GoogleHistoryStopWord(), new MySQLStopWord() );

    private Option option = new Option();

	public SimpleEngine() {
	}
		
	public Result createCards( String article, String source ) {
		return this.createCards(article, source, this.option);
	}

	public Result createCards( String article, String source, Option option ) {
		this.option = option;
		List<Sentence> sentences = this.createSentecesFromArticle( article, source );
		return this.createCardsImpl(sentences, source);
	}

	public List<KeywordResult> createKeywords( String article, String source, Option option ) {
		// Article To Sentences
		this.option = option;
		List<Sentence> sentences = this.createSentecesFromArticle( article, source );
		
		Pair<List<Keyword>, List<Sentence>> pair = this.generateKeywordsAndSentences(sentences, source);
		List<Keyword> keywords = pair.first();
		List<KeywordResult> result = 
				keywords.parallelStream()
				        .filter(x -> x.getNumberOfOccurrence() > 0)
				        .sorted((x1, x2) -> Integer.compare(x2.getNumberOfOccurrence(), x1.getNumberOfOccurrence()))
				        .map(KeywordResult::new)
				        .collect(Collectors.toList());
		return result;
	}

	private Result createCardsImpl( List<Sentence> sentences, String source) {
		Pair<List<Keyword>, List<Sentence>> pair = this.generateKeywordsAndSentences(sentences, source);
		List<Keyword> keywords = pair.first();
		sentences = pair.second();

		// Sort Sentences by Scores
		// Cut-off sentences
		sentences = this.sentenceSortAndCutoff( sentences, option );
		
		// Determine questions
		List<Keyword> key_pool = new ArrayList<Keyword>();
		for( Sentence s : sentences ) {
			s.defineQuestion(keywords, key_pool);
		}
				
//		// print out
//		System.out.println("KEYWORDS (" + keywords.size() + "): " );
//		for  ( int j = 0; j < keywords.size(); ++j ) {
//			Keyword k = keywords.get(j);
//			System.out.println(k.getText());
//			System.out.println("  IS_PLURAL=" + k.isPlural());
//			System.out.println("  SINGULAR=" + k.getTextSingular());
//			System.out.println("  PLURAL=" + k.getTextPlural() );
//		}
//		System.out.println();

//		for( Sentence s: sentences ) {
//			System.out.println(s.toString(true));
//			System.out.println("DECOYS = " + s.getQuestionDefine().decoys);
////			System.out.println("LEMMA = " + lemma.lemmatize(s.getText()));
////			System.out.print("PARSE = ");
////			s.getTree().pennPrint();
//			System.out.println("SCORE = " + s.getScore() );
//			System.out.println("KEYWORDS = ");
//			for (Keyword k : s.getKeywords() ) {
//				System.out.println( "  " + k.getText() + " --> LEMMA = " + k.getLemma() + " PLURALS = " + s.getKeywordOccurrencePlurals(k));
//			}
//			System.out.println();
//		}
//		
//		System.out.println("NUM OF SENTENCES = " + sentences.size());
		
		// Create Cards
		List<Card> cards = new ArrayList<Card>();
		// FIB
		cards.addAll( sentences.stream()
							   .parallel()
							   .map(CardFillInBlank::new)
							   .collect(Collectors.toList()) );
		
		// Multiple
		cards.addAll( sentences.stream()
							   .parallel()
							   .filter((x) -> x.getQuestionDefine().decoys.size() >= SETTINGS.NUM_CHOICE)
							   .map(CardMultiChoice::new)
							   .collect(Collectors.toList()) );
		// True / False
		cards.addAll( sentences.stream()
							   .parallel()
							   .filter((x) -> x.getQuestionDefine().decoys.size() > 0)
							   .map(CardTrueFalse::new)
							   .collect(Collectors.toList()) );
		
		return new Result(cards, sentences);		
	}

	private Pair<List<Keyword>, List<Sentence>> generateKeywordsAndSentences( List<Sentence> sentences, String source) {		
		// Task 1: get keywords and update freq & scores
		//
		List<String> keywordStrs = new ArrayList<String>();
		final List<Sentence> sents = sentences;		
		Collection<Callable<Object>> tasks = new ArrayList<Callable<Object>>();
		tasks.add(() -> {
			// Re-merge to article
			String re_article = "";
			for( Sentence s: sents ) {
				re_article += s.getText() + " ";
			}

			// Find Keywords
			return findKeywords(re_article, source);
		});
		
		// Task 2: build parse tree
		//
		tasks.add( () -> {
			sentenceBuildTree( sents );
			return null;
		});
		
		// Run tasks
		//
		try {
			List<Future<Object>> results = workers.invokeAll(tasks);
			keywordStrs = (List<String>) results.get(0).get();
			results.get(1).get();
		} catch( Exception e ) {
			e.printStackTrace();
		}
		
		// NER keywords from sentences
		ArrayList<String> ner_strs = new ArrayList<String>();
		for( Sentence s : sents ) {
			for( Triple<String, Integer, Integer> ent : s.getNERSpans() ) {
				int beg_id = ent.second;
				int end_id = ent.third;
				String ner_text = s.getText().substring(beg_id, end_id);
				ner_strs.add(ner_text);
			}
		}
		
		// update merged keywords
		ArrayList<List<String>> keySets = new ArrayList<List<String>>();
		keySets.add( keywordStrs );
		keySets.add( ner_strs );
		if (this.option.keygenUsePrepFlash) {
			// PrepFlash keywords from sentences
			Set<String> pfKeySet = this.findPrepFlashKeywords(sents);
			keySets.add( new ArrayList<String>(pfKeySet) );
		}
		keywordStrs = this.mergeKeywordSets(keySets);
		
		// create Keyword objects
		List<Keyword> keywords = keywordStrs.stream().map(Keyword::new).collect(Collectors.toList());
		this.keywordBuildTree(keywords);
		
		keywords  = this.keywordFilterByTree(keywords);
		List<Sentence> postSentences = this.sentencesFilterByTree( sents, option );

		// Update freq. and scores
		this.updateKeywordFrequency( keywords, postSentences );
		this.updateSentenceScore( keywords, postSentences );
		return new Pair<List<Keyword>, List<Sentence>>(keywords, postSentences);
	}
	
	// Algorithms
		
	private void updateKeywordFrequency( List<Keyword> keywords, List<Sentence> sentences ) {		
		// sorted by length: Long --> Short
		ArrayList<Keyword> sorted_keys = new ArrayList<Keyword>(keywords);
		Collections.sort(sorted_keys, (k1, k2) -> Integer.compare(k2.getText().length(), k1.getText().length()) );
		
		sentences.parallelStream()
				 .peek((x) -> {
					 for  ( int j = 0; j < sorted_keys.size(); ++j ) {
						 x.putKeyword( sorted_keys.get(j) );
					 }
				 })
				 .count();
		
		// keyword frequency
		int[] key_occr = new int[keywords.size()];
		Arrays.fill(key_occr, 0);

		for ( int i = 0; i < sentences.size(); ++i ) {
			Sentence sent = sentences.get(i);
			for  ( int j = 0; j < sorted_keys.size(); ++j ) {
				key_occr[j] += sent.getKeywordOccurrenceNumber( sorted_keys.get(j) );
			}
		}

		for ( int j = 0; j < sorted_keys.size(); ++j ) {
			sorted_keys.get(j).setNumberOfOccurrence(key_occr[j]);
		}
	}
	
	private void updateSentenceScore( List<Keyword> keywords, List<Sentence> sentences ) {
		for ( int i = 0; i < sentences.size(); ++i ) {
			Sentence sent = sentences.get(i);
			double score = 0.0;
			int num_key_of_sent = 0;
			for  ( int j = 0; j < keywords.size(); ++j ) {
				boolean contain_keyword = sent.getKeywordOccurrenceNumber( keywords.get(j) ) > 0;
				if (contain_keyword) {
					score += keywords.get(j).getNumberOfOccurrenceInverse();
					++num_key_of_sent;
				}
			}
			//score /= (double)keywords.size();
			if( num_key_of_sent > 0 )
				score /= (double)num_key_of_sent;
			sent.setScore( score );
		}
	}
	
	// Sentence
	
	private List<Sentence> sentencesPreprocessing( List<Sentence> sents, String source ) {
		sents = this.mergeExtremelyShortSentences(sents);
		sents = sents.stream()
					 .parallel()
					 .filter((s) -> s.getText().length() < Configuration.SETTINGS.MAX_SENTENCE_LENGTH)
					 .filter((s) -> s.getPreviousSentence() == null? true : s.getPreviousSentence().length() < Configuration.SETTINGS.MAX_SENTENCE_LENGTH)
					 .filter((s) -> s.numberOfTokensApproximation() > Configuration.SETTINGS.MIN_SENTENCE_TOKENS
							  	&& !s.getText().matches(".*== .{1,60} ==.*") ) // remove wiki title
					 .peek((s) -> {
						 s.setText( s.getText().replaceAll("\\[[0-9]+\\]$", "") ); // remove wiki suffix
						 s.setSource( source );
					 }) 
					 .collect(Collectors.toList());
		return sents;
	}
	
	private List<Sentence> createSentecesFromArticle( String article, String source ) {
		if (option.skipSentencePreprocessing) {
			String[] articleSentences = article.split("\n");
			List<Sentence> sentences = this.createSentecesFromArticle( Arrays.asList(articleSentences), true );
			// filter out empty sentences
			sentences = this.sentencesFilter(sentences, new Predicate<Sentence>() {
				public boolean test(Sentence t) {
					return t.getText().trim().length() > 0;
				}
			});
			return sentences;			
		} else {
			// Article To Sentences
			SentenceDetector sd = new SentenceDetector();
			List<Sentence> sentences = this.createSentecesFromArticle( sd.process(article), false );
			sentences = this.sentencesPreprocessing( sentences, source );
			return sentences;			
		}
	}

	private List<Sentence> createSentecesFromArticle( List<String> articleSentences, boolean addSentenceId ) {
		ArrayList<Sentence> sents = new ArrayList<Sentence>();
		for( int i = 0; i < articleSentences.size(); ++i ) {
			Integer sentId = addSentenceId ? i : null;
			if (i == 0) {
				sents.add( new Sentence(sentId, articleSentences.get(i)) );
			} else {
				sents.add( new Sentence(sentId, articleSentences.get(i), articleSentences.get(i-1)) );
			}
		}
		return sents;		
	}

	private List<Sentence> mergeExtremelyShortSentences( List<Sentence> sents ) {
		ArrayList<Sentence> r = new ArrayList<Sentence>();
		for( int i = 0; i < sents.size()-1; ++i ) {
			Sentence s = sents.get(i);
			if (s.numberOfTokensApproximation() < Configuration.SETTINGS.MIN_SENTENCE_TOKENS ) {
				String u = s.getText();
				String v = sents.get(i+1).getText();
				s.setText( u + v );
				++i;
			}
			r.add(s);
		}
		return r;
	}

	private List<Sentence> sentencesFilter( List<Sentence> sents, Predicate<? super Sentence> pred) {
		List<Sentence> r = sents.stream()
				.parallel()
				.filter(pred)  // filtering will be performed concurrently
				.collect(Collectors.toCollection(ArrayList::new));
		return r;
	}
		
	private List<Sentence> sentencesFilterByTree( List<Sentence> sentences, Option option ) {
		List<Sentence> sents = this.sentencesFilter( sentences, new Predicate<Sentence>() {
			@Override
			public boolean test(Sentence t) {
				Tree top = t.getTree();
				List<Tree> leaves = top.getLeaves();
				if (leaves.size() > 0 ) {
				    Tree node = leaves.get(0).parent(top);
				    boolean isPRP = node.nodeString().startsWith("PRP");
				    if (!isPRP && node.nodeString().startsWith("DT")) {
					    Tree parent = node.parent(top);
					    isPRP |= parent.numChildren() == 1;
				    }
				    boolean isSentence = true;
				    if (top.numChildren() > 0) {
				    	isSentence = top.getChild(0).nodeString().startsWith("S");
				    }
				    if( isPRP ) {
				    	t.addAnnotation("PRP");
				    }
				    if( !isSentence ) {
				    	t.addAnnotation("FRAGMENT");
				    }
				    if (!option.useDebug()) {
				    	return !isPRP && isSentence;
				    }
				}
				
				// DEBUG: put parse tree to annotation
				if( option.useDebug() ) {
					t.addAnnotation( top.pennString() );
				}

				return option.useDebug();
			}
		});
		
		
		return sents;
	}

	private long sentenceBuildTree( List<Sentence> sentences ) {
		return sentences.stream()
				 .parallel()
				 .peek( (x) -> {
				    Tokenizer<? extends HasWord> toke = tlp.getTokenizerFactory().getTokenizer(new StringReader(x.getText()));
				    List<? extends HasWord> s2 = toke.tokenize();
				    List<TaggedWord> tagged = tagger.tagSentence(s2);
				    Tree tree = model.apply(tagged);
				    x.setTree(tree);
				    x.setTaggedWords(tagged);
				    // lemma
				    List<String> lem = lemma.lemmatize(x.getText());
				    x.setLemma(lem);
				    // ner
				    if (this.option.keygenUseNER) {
					    List<String> ner = lemma.ner(x.getText());
					    x.setNER(ner);
					    x.buildNERSpans();
				    }
				 } )
				 .count();
	}

	private List<Sentence> sentenceSortAndCutoff( List<Sentence> sentences, Option option ) {
		// Sort Sentences by Scores (High-->Low)
		Collections.sort( sentences );

		// Cut-out sentences without keywords
		List<Sentence> sent_r = this.sentencesFilter(sentences, (s) -> {
			boolean pick = true;
			pick &= s.getKeywords().size() > 0;
			return pick;
		});

		// Cut-off scores
		if ( sent_r.size() <= Configuration.SETTINGS.SENTENCE_NUM_FOR_CUTOFF ) {
			return sent_r;
		} else {
			Double[] acmult = sentences.stream().map((x) -> x.getScore()).toArray(Double[]::new);
			for( int i = 1; i < acmult.length; ++i )
				acmult[i] += acmult[i-1];
			double max = acmult[ acmult.length - 1 ];
			double cut_off = 0.4 * max;
			int j = 0;
			for( ; j < acmult.length; ++j )
				if( acmult[j] > cut_off )
					break;
			int max_id = Math.max(j, Configuration.SETTINGS.SENTENCE_NUM_FOR_CUTOFF);
			if (!option.useDebug()) {
				sent_r = sent_r.subList(0, max_id);
			} else {
				for( int q = max_id; q < sent_r.size(); ++q ) {
					sent_r.get(q).addAnnotation("LOW_SCORE");
				}
			}
			return sent_r;
		}
		
//		// Cut-off scores
//		if ( sent_r.size() <= Configuration.SETTINGS.SENTENCE_NUM_FOR_CUTOFF ) {
//			return sent_r;
//		} else {
//			Double[] dxdx = sentences.stream().map((x) -> x.getScore()).toArray(Double[]::new);
//			for( int n = 0; n < 2; ++n ) {
//				for( int i = sent_r.size()-1; i > 0; --i ) {
//					dxdx[i] -= dxdx[i-1];
//				}
//				dxdx[0] = dxdx[1];
//			}
//			dxdx = Arrays.stream(dxdx).map((x) -> Math.abs(x)).toArray(Double[]::new);
//			
//			int i = Configuration.SETTINGS.SENTENCE_NUM_FOR_CUTOFF;
//			double max_dxdx = dxdx[i];
//			int max_id = i;
//			for( ++i; i < dxdx.length; ++i ) {
//				if( dxdx[i] > max_dxdx ) {
//					max_dxdx = dxdx[i];
//					max_id = i;
//				}
//			}
//			if (!option.useDebug()) {
//				sent_r = sent_r.subList(0, max_id);
//			} else {
//				for( int q = max_id; q < sent_r.size(); ++q ) {
//					sent_r.get(q).addAnnotation("LOW_SCORE");
//				}
//			}
//			return sent_r;
//		}

	}
	
	// Keywords
	
	@SuppressWarnings("unchecked")
	private List<String> findKeywords( String article, String source ) {		
		Collection<Callable<List<String>>> tasks = new ArrayList<Callable<List<String>>>();
	
		// Alchemy keywords
		if ( this.option.keygenUseAlchemy && !article.isEmpty() ) {
			tasks.add(new Callable<List<String>>() {
				public List<String> call() {
					return findAlchemyAPIKeywords(article);
				}});
		}
		
		// Wiki linked keywords
		if ( this.option.keygenUseWiki && source.contains("en.wikipedia.org/wiki") ) {
			tasks.add(new Callable<List<String>>() {
				public List<String> call() {
					return findWikiLinkedKeywords(source);
				}});
		}
		
		try {
			List<Future<List<String>>> results = this.workers.invokeAll(tasks);
			ArrayList<List<String>> keySets = new ArrayList<List<String>>();
			for( Future<List<String>> r : results ) {
				keySets.add(r.get());
			}

			// Smile (co-occurrence matrix) keywords
			if ( this.option.keygenUseSmile && !article.isEmpty() ) {
				keySets.add(findCooccurrenceKeywords(article));
			}

			List<String> mergedKeys = this.mergeKeywordSets( keySets );
		    return mergedKeys;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ArrayList<String>();
	}
	
	private void keywordBuildTree( List<Keyword> keywords ) {
		// lemma
		int dummy = (int)
			keywords.stream()
					.parallel()
					.peek( (x) -> {
					    Tokenizer<? extends HasWord> toke = tlp.getTokenizerFactory().getTokenizer(new StringReader(x.getText()));
					    List<? extends HasWord> s2 = toke.tokenize();
					    List<TaggedWord> tagged = tagger.tagSentence(s2);
					    Tree tree = model.apply(tagged);
					    x.setTree(tree);
					    x.setTaggedWords(tagged);
					    // lemma
						List<String> lem = lemma.lemmatize(x.getText());
						x.setLemma(lem);
					})
					.count();
	}
	
	@SuppressWarnings("unchecked")
	private List<String> mergeKeywordSets( List<List<String>> sets ) {
		TreeSet<String> key = new TreeSet<String>();
		for( List<String> keyset: sets ) {
			for( String k : keyset) {
				boolean picked = true;
				picked &= k.length() > Configuration.SETTINGS.MIN_KEYWORD_LENGTH;
				picked &= k.length() < Configuration.SETTINGS.MAX_KEYWORD_LENGTH;
				picked &= !k.matches("[*?><!+]()"); // ignore special chars
				if (picked) {
					key.add(k);
				}
			}
		}
		return new ArrayList<String>( key );
	}
	
	private List<String> findAlchemyAPIKeywords( String article ) {
		String[] results = AlchemyAPIWrapper.TextGetRankedKeywords(article);
		List<String> keys = 
				Arrays.stream(results)
					  .parallel()
					  .map( (k) -> k.replaceAll("^\\s+|\\s+$|\\.+$", "") ) // trim & remove end periods
					  .collect( Collectors.toList() );
		// System.out.println("Alchemy KEYWORDS: " + keys);		
		return keys;
	}
	
	private List<String> findCooccurrenceKeywords( String article ) {
		int maxKeyNum;
		if (this.option.keygenOptionSmileMaxKeyNum > 0) {
			maxKeyNum = this.option.keygenOptionSmileMaxKeyNum;
		} else {
			int apprxTokens = article.split("\\s+").length;
			maxKeyNum = apprxTokens > 1000 ? apprxTokens / 100 : 10;			
		}

		List<NGram> grams = this.smileKeywordGen.extract(article, maxKeyNum);
		List<String> keys = 
				grams.stream()
				     .parallel()
				     .map( (x) -> String.join(" ", x.words))
				     .collect( Collectors.toList() );
		System.out.println("Cooccurrence KEYWORDS: " + keys);
		return keys;
	}

	private List<String> findWikiLinkedKeywords( String wikiURL ) {
		if (wikiURL == null) {
			return new ArrayList<String>();
		} else if (!wikiURL.contains("en.wikipedia.org/wiki")) {
			return new ArrayList<String>();
		}
		
		// fetch wiki HTML
		try {
			Document doc = Jsoup.connect(wikiURL).get();
			Elements nodes = doc.select("p a");
			List<String> keys = 
				nodes.stream()
					 .parallel()
					 .map( (x) -> x.text() )
					 .filter( (x) -> !x.isEmpty() )
					 .filter( (x) -> !x.matches(".*\\[.*\\].*") )
					 .filter( (x) -> !x.matches("^\\/") )
					 .map( (x) -> x.replaceAll("^\\s+|\\s+$", "") ) // trim
					 .map( (x) -> x.replaceAll("\\s*\\(.*\\)\\s*$", "") )
					 .collect(Collectors.toList());
			// System.out.println("Wiki KEYWORDS: " + keys);
			return keys;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}
	
	private Set<String> findPrepFlashKeywords( List<Sentence> sents ) {
		return sents.stream()
				    .parallel()
				    .map((x) -> {
				    	HashSet<String> keys = new HashSet<String>();
				    	this.findPrepFlashKeywords(x.getTree(), keys);
				    	return keys;
				    })
				    .flatMap((x) -> x.stream())
				    .collect(Collectors.toSet());
	}

	private void findPrepFlashKeywords( Tree tree, HashSet<String> keys) {
		if (tree.value().startsWith("NP")) {
			ArrayList<String> keyNodes = new ArrayList<String>();
			for( Tree child: tree.getChildrenAsList() ) {
				String word = child.pennString();
				word = word.substring(1, word.length() - 2);
				word = word.split("\\s+")[1];
				
				boolean isLeaf = child.getChildrenAsList().size() == 1;
				boolean isStop = this.swchk.isStopWord(word);
				boolean ok = isLeaf && !isStop && (child.value().startsWith("NN") || child.value().startsWith("JJ"));
				if (ok) {
					keyNodes.add(word);
				} else {
					if (keyNodes.size() > 0) {
						String key = String.join(" ", keyNodes);
						keys.add(key);
						keyNodes.clear();
					}
					if (!isLeaf) {
						this.findPrepFlashKeywords(child, keys);
					}
				}
			}
			// clean up stacks
			if (keyNodes.size() > 0) {
				String key = String.join(" ", keyNodes);
				keys.add(key);
				keyNodes.clear();
			}
		} else {
			for( Tree child: tree.getChildrenAsList() ) {
				if (!child.isLeaf()) {
					this.findPrepFlashKeywords(child, keys);
				}
			}
		}
	}

	private List<Keyword> keywordFilterByTree( List<Keyword> keywords ) {
		return keywords.stream()
					   .parallel()
					   .filter((x) -> x.getTree().getChild(0).value().startsWith("NP"))
					   .collect(Collectors.toList());
	}

}
