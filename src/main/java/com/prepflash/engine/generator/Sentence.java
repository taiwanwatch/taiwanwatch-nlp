package com.prepflash.engine.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.prepflash.engine.common.Configuration;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.Pair;
import edu.stanford.nlp.util.Triple;

public class Sentence implements Comparable<Sentence> {
	
	static public class QuestionDefine {
		public Keyword keyword;
		public Pair<Integer, Integer> span;
		public boolean is_plural;
		public ArrayList<String> decoys;
	}
	
	private Integer id = null;
	private String text;
	private String prev;
	private String source;
	// keyword <--> spans map (multiple spans with [beg, end], [beg, end]... idx)
	private HashMap<Keyword, List<Pair<Integer, Integer>>> keyword_occrs = new HashMap<Keyword, List<Pair<Integer, Integer>>>();
	private HashMap<Keyword, List<Boolean>> keyword_plural = new HashMap<Keyword, List<Boolean>>(); // true: plural; false: singular
	private double score = 0.0;
	private int numTokenApprox = 0;
	private Tree tree;
	private List<TaggedWord> taggedWord;
	private List<String> lemma;
	private List<String> ner;
	private QuestionDefine ques;
	private List<String> annotations = new ArrayList<String>();
	// [NER_type, begin_Id, end_Id]
	private ArrayList<Triple<String, Integer, Integer>> ner_spans = new ArrayList<Triple<String, Integer, Integer>>();
	
	public Sentence( Integer id, String text, String prev ) {
		this.id = id;
		this.setText(text);
		this.prev = prev;
	}

	public Sentence( Integer id, String text ) {
		this.id = id;
		this.setText(text);
	}
	
	public Integer getId() {
		return this.id;
	}

	public int putKeyword( Keyword key) {
		List<String> lma_sent = this.lemma;
		List<String> lma_keys = key.getLemma();
		ArrayList<Pair<Integer, Integer>> spans = new ArrayList<Pair<Integer, Integer>>();
		ArrayList<Boolean> plurals = new ArrayList<Boolean>();
		
		ArrayList<Pair<Integer, Integer>> all_spans = new ArrayList<Pair<Integer, Integer>>();
		for( List<Pair<Integer, Integer>> pair_list : this.keyword_occrs.values() ) {
			all_spans.addAll(pair_list);
		}
		
		int index = 0;
		while ( index + lma_keys.size() <= lma_sent.size() ) {
			boolean in = true;
			List<String> sublemma = this.lemma.subList(index, index+lma_keys.size());
			for( int i = 0; i < lma_keys.size(); ++i ) {
				in &= lma_keys.get(i).equals( sublemma.get(i) );
			}			
			if(in) { // exatract the original spans
				List<TaggedWord> subtags = this.taggedWord.subList(index, index+lma_keys.size());
				int beg = subtags.get(0).beginPosition();
				int end = subtags.get( subtags.size()-1 ).endPosition();
				Pair<Integer, Integer> span = new Pair<Integer, Integer>(beg, end);
				if (!this.checkOverlap(all_spans, span)) {
					spans.add(span);
					// plural or singular ?
					boolean singular = true;
					for( int i = 0; i < sublemma.size(); ++i ) {
						String lem = sublemma.get(i);
						String txt = subtags.get(i).value();
						singular &= lem.toUpperCase().equals( txt.toUpperCase() ); // case insensitive
					}
					if( singular ) {
						// FIXME: probably not singular. could be mass
						// require further checking by looking at POS noun tags
					}
					plurals.add(!singular);
					index += lma_keys.size();
				} else {
					++index;
				}
			} else {
				++index;
			}
		}
		
		// update keyword_occrs
		if ( spans.size() > 0 ) {
			this.keyword_occrs.put(key, spans);
			this.keyword_plural.put(key, plurals);
		}
		return spans.size();
	}
		
	public void defineQuestion( List<Keyword> key_all, List<Keyword> key_pool ) {
		QuestionDefine q = new QuestionDefine();
		Set<Keyword> keys = this.getKeywords();
		Optional<Keyword> key_best = 
				keys.stream()
					.reduce((x1, x2) -> x1.getNumberOfOccurrence() > x2.getNumberOfOccurrence() ? x2 : x1 );
		if( key_best.isPresent() ) {
			Keyword k = key_best.get();
			q.keyword = k;
			q.span = this.getKeywordOccurrenceSpans(k).get(0); // FIXME: get first is not always the best choice
			q.is_plural = this.getKeywordOccurrencePlurals(k).get(0);
			this.ques = q;
			this.makeDecoys( key_all, key_pool );
			return;
		}
	}
	
	public void buildNERSpans() {
		String prevNERToken = null;
		ArrayList<Integer> idx = new ArrayList<Integer>();
		for( int i = 0; i < this.ner.size(); ++i ) {
			String nerToken  = this.ner.get(i);
			if( !nerToken.equals("O") 
			 && (nerToken.equals(prevNERToken) || prevNERToken == null) ) {
				idx.add(i);
				prevNERToken = nerToken;
				continue;
			} else if( idx.size() > 0 ) {
				if( prevNERToken != null ) {
					int beg_id = idx.get(0);
					int end_id = idx.get(idx.size()-1);
					List<TaggedWord> subtags = this.taggedWord.subList(beg_id, end_id+1);
					int beg = subtags.get(0).beginPosition();
					int end = subtags.get( subtags.size()-1 ).endPosition();
					this.ner_spans.add( new Triple<String, Integer, Integer>( prevNERToken, beg, end ) );
				}
				prevNERToken = null;
				idx.clear();
				
				if( !nerToken.equals("O") ) {
					idx.add(i);
					prevNERToken = nerToken;
				}
			}
		}
		
		if( idx.size() > 0 && prevNERToken != null ) {
			List<TaggedWord> subtags = this.taggedWord.subList(idx.get(0), idx.get(idx.size()-1));
			if( subtags.size() > 0 ) {
				int beg = subtags.get(0).beginPosition();
				int end = subtags.get( subtags.size()-1 ).endPosition();
				this.ner_spans.add( new Triple<String, Integer, Integer>( prevNERToken, beg, end ) );
			}
		}
		
		// dump
		/*
		for( Triple<String, Integer, Integer> trp : this.ner_spans ) {
			int beg = trp.second();
			int end = trp.third();
			String word = this.getText().substring(beg, end);
			String nerToken = trp.first();
			System.out.println(nerToken + " --> " + word);
		}
		*/
	}

	public QuestionDefine getQuestionDefine() {
		return this.ques;
	}
	
	public Set<Keyword> getKeywords() {
		return this.keyword_occrs.keySet();
	}

	public List<Boolean> getKeywordOccurrencePlurals( Keyword k ) {
		return this.keyword_plural.get(k);
	}

	public List<Pair<Integer, Integer>> getKeywordOccurrenceSpans( Keyword k ) {
		return this.keyword_occrs.get(k);
	}

	public int getKeywordOccurrenceNumber( Keyword keyword ) {
		List<Pair<Integer, Integer>> spans =  this.keyword_occrs.get(keyword);
		return spans == null ? 0 : spans.size();
	}

	public void addAnnotation( String a ) {
		this.annotations.add(a);
	}
	
	public List<String> getAnnotations() {
		return this.annotations;
	}
	
	public void setScore( double s ) {
		this.score = s;
	}
	
	public double getScore() {
		return this.score;
	}
	
	// Tree
	public void setTree( Tree p ) {
		this.tree = p;
	}
	
	public Tree getTree() {
		return this.tree;
	}

	// TaggedWord
	public void setTaggedWords( List<TaggedWord> p ) {
		this.taggedWord = p;
	}
	
	public List<TaggedWord> getTaggedWords() {
		return this.taggedWord;
	}

	// Lemma
	public void setLemma( List<String> p ) {
		this.lemma = p;
	}
	
	public List<String> getLemma() {
		return this.lemma;
	}

	// NER
	public void setNER( List<String> p ) {
		this.ner = p;
	}
	
	public List<String> getNER() {
		return this.ner;
	}

	public List<Triple<String, Integer, Integer>> getNERSpans() {
		return this.ner_spans;
	}
	
	// Text
	public String getText() {
		return this.text;
	}
	
	public void setText( String text ) {
		this.text = text;
		this.numTokenApprox = this.text.split("\\s+").length;
	}
		
	public String getPreviousSentence() {
		return this.prev;
	}
	
	public int numberOfTokensApproximation() { 
		return this.numTokenApprox;
	}

	public String getSource() {
		return this.source;
	}
	
	public void setSource( String source ) {
		this.source = source;
	}
	
	@Override
	public int compareTo(Sentence o) {
		return Double.compare(o.score, this.score); // score high --> low
	}

	@Override
	public String toString() {
		return this.text;
	}
	
	public String toString( boolean replaceKeywordToUpperCase ) {
		String str = this.text;
		for( Map.Entry<Keyword, List<Pair<Integer, Integer>>> kv: this.keyword_occrs.entrySet() ) {
			for( Pair<Integer, Integer> span : kv.getValue() ) {
				String sub = str.substring(span.first, span.second);
				str = str.substring(0, span.first) + sub.toUpperCase() + str.substring(span.second, str.length());
			}
		}
		return str;
	}
	
	private boolean checkOverlap( List<Pair<Integer, Integer>> all_spans, Pair<Integer, Integer> span ) {
		// check if span has been taken
		int beg_i = span.first;
		int end_i = span.second;
			for( Pair<Integer, Integer> pair_j : all_spans ) {
				int beg_j = pair_j.first;
				int end_j = pair_j.second;
				if ( (beg_i >= beg_j && beg_i <  end_j) 
				  || (end_i >  beg_j && end_i <= end_j) ) 
				{
					// overlap--span has been taken;
					return true;
				}
			}
		return false;
	}
	
	private void makeDecoys( List<Keyword> key_all, List<Keyword> key_pool ) {
		ArrayList<Keyword> key_avail = new ArrayList<Keyword>(key_all);
		key_avail.remove(this.ques.keyword);
		ArrayList<Keyword> decoy_keys = new ArrayList<Keyword>();
		for( int n = 0; n < Configuration.SETTINGS.MAX_NUM_DECOY; ++n ) {
			if ( key_avail.size() == 0 ) {
				break;
			}
			Keyword pick = null;
			for( int i = 0; i < Configuration.SETTINGS.MAX_DECOY_TRY; ++i ) {
				int rndid = (int)(Math.random() * key_avail.size());
				pick = key_avail.get(rndid);				
				if (!key_pool.contains( pick )) {
					break;
				}
			}
			key_avail.remove(pick);
			decoy_keys.add(pick);
		}
		
		this.ques.decoys = new ArrayList<String>();		
		for( Keyword pick : decoy_keys ) {
			if( this.ques.is_plural && !pick.isPlural() ) {
				this.ques.decoys.add( pick.getTextPlural() );
			} else if( !this.ques.is_plural && pick.isPlural() ) {
				this.ques.decoys.add( pick.getTextSingular() );
			} else {
				this.ques.decoys.add( pick.getText() );
			}
		}		
		key_pool.add(this.ques.keyword);
						
		// capitalize first char if needed
		if (this.ques.span.first == 0) {
			String sub = this.text.substring(this.ques.span.first, this.ques.span.second);
			String cap_semi = "[^\\sA-Z]+[A-Z]+.*"; // e.g., tRNA
			String cap_strict = "^[A-Z]+.*"; // strict capital first
			if( sub.matches(cap_semi) || sub.matches(cap_strict) ) { // to Upper
				this.ques.decoys = this.ques.decoys
						.stream()
						.map((x) -> x.matches(cap_semi) ? 
									x : 
									x.substring(0, 1).toUpperCase() + x.substring(1, x.length()))
						.collect(Collectors.toCollection(ArrayList::new)); 
			}
		}
	}
}