package com.prepflash.engine.generator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.prepflash.engine.common.Configuration;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;

public class SentenceDetector {
	
	private SentenceDetectorME sentenceDetector = null;
	
	public SentenceDetector() {
		try {
			InputStream modelIn = new FileInputStream(Configuration.SETTINGS_PCKG.OPENNLP_MODEL_SENT);
			SentenceModel model = new SentenceModel(modelIn);
			this.sentenceDetector = new SentenceDetectorME(model);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
	}
	
	public List<String> process( String str ) {
		str = SentenceDetector.concatenateSentences(str);
		String[] s = this.sentenceDetector.sentDetect(str);
		return Arrays.stream(s)
					 .parallel()
					 .flatMap( (x) -> Arrays.stream( x.split("\n") ) )
					 .collect( Collectors.toList() );
	}
	
	public static String concatenateSentences( String str ) {
		Function<String, String> trimRight = (s) -> "" + s.trim();
		Function<String, String> trimLeft  = (s) -> s.trim() + "";
		
		BiFunction<String, String, String> func = (s1, s2) -> {
			String s1R = trimRight.apply(s1);
			String s2L = trimLeft.apply(s2);
			if (s1R.endsWith("-") || s2L.startsWith("-") ) {
				return s1R + s2L;
			} else {
				return s1R + " " + s2L;
			}
		};
		
		return Arrays.stream( str.split("\n") )
					 .parallel()
					 .reduce( new String(), 
							 (lump, s) -> func.apply( lump, s ), 
							 (s1,  s2) -> func.apply( s1, s2 ) );
	}
}
