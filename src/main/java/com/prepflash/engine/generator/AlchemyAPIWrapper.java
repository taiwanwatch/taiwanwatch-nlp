package com.prepflash.engine.generator;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import com.prepflash.engine.common.Configuration;

public class AlchemyAPIWrapper {
	private static String API_KEY = Configuration.SETTINGS_PCKG.ALCHEMY_API_KEY;
	private static String API_SERVER = Configuration.SETTINGS_PCKG.ALCHEMY_API_SERVER;
	
	private static JSONObject POST( String urlStr, String article, String options ) {
		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);

	        StringBuilder data = new StringBuilder();

	        data.append("apikey=").append(API_KEY);
	        data.append("&outputMode=json&text=" + URLEncoder.encode(article, "UTF-8") + "&maxRetrieve=500");
	        if ( options != null ) {
	        	data.append(options);
	        }

	        conn.addRequestProperty("Content-Length", Integer.toString(data.length()));
	        
	        DataOutputStream ostream = new DataOutputStream(conn.getOutputStream());
	        ostream.write(data.toString().getBytes());
	        ostream.close();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String line = null;
			String output = "";
			while ((line = br.readLine()) != null) {
				output += line;
			}			
			conn.disconnect();
			return new JSONObject(output);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {		 
			e.printStackTrace();
		} 
		return null;
	}
	
	public static String[] TextGetRankedKeywords( String article ) {	
		JSONObject json = POST( API_SERVER + "calls/text/TextGetRankedKeywords", article, "&keywordExtractMode=strict" );
		try {
			JSONArray keys = json.getJSONArray("keywords");
			String [] key_texts = new String[keys.length()];
			for ( int i =0; i < keys.length(); ++i ) {
				JSONObject text_relv = keys.getJSONObject(i);
				String key_text = text_relv.getString("text");
				double key_relv = Double.parseDouble( text_relv.getString("relevance") );
				key_texts[i] = key_text;
			}
			return key_texts;
		} catch( Exception e ) {
			e.printStackTrace();
		}
		return new String[0];
	}
	
	public static void TextGetRankedNamedEntities( String article ) {	
		JSONObject json = POST( API_SERVER + "calls/text/TextGetRankedNamedEntities", article, null );
		JSONArray ents = json.getJSONArray("entities");
		for ( int i =0; i < ents.length(); ++i ) {
			JSONObject ent_json = ents.getJSONObject(i);
			System.out.println( ent_json.toString(2) );
		}
	}

}
