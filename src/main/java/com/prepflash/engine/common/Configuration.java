package com.prepflash.engine.common;

public class Configuration {

	public static class SETTINGS {
		public static final int MAX_SENTENCE_LENGTH = 512;
		public static final int MIN_SENTENCE_TOKENS = 3;
		public static final int MIN_KEYWORD_LENGTH = 3;
		public static final int MAX_KEYWORD_LENGTH = 40;
		public static final int MAX_NUM_DECOY = 20;
		public static final int MAX_DECOY_TRY = 10;
		public static final int NUM_CHOICE = 4; // number of choice per each multiple-choice card
		public static final int SENTENCE_NUM_FOR_CUTOFF = 20; // total number of sentences larger than this number, engine will apply cutoff algorithm
	}

	public static class SETTINGS_PCKG {
		public static final String NLP_MODEL_PREFIX = "/Users/liao/Dropbox/Sybala/engine/libs/model/";
		
		// Stanford NLP
		public static final String STANFORD_NLP_GRAMMAR = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
		public static final String STANFORD_NLP_MODEL_PATH = "edu/stanford/nlp/models/srparser/englishSR.ser.gz";
		public static final String STANFORD_NLP_TAGGER_PATH = NLP_MODEL_PREFIX + "english-left3words-distsim.tagger";
		
		// OpenNLP
		public static final String OPENNLP_MODEL_SENT = NLP_MODEL_PREFIX + "en-sent.bin";
		public static final String OPENNLP_MODEL_PARSE = NLP_MODEL_PREFIX + "en-parser-chunking.bin";
		
		// Alchemy API
		public static final String ALCHEMY_API_KEY = "08a9275f07dafbe3bff17290627fa6de8f4a638e";
		public static final String ALCHEMY_API_SERVER = "http://access.alchemyapi.com/";
		
		// PrepFlash API
		public static final String PREPFLASH_API_USERNAME = "engine";
		public static final String PREPFLASH_API_PASSWROD = "engine5566forever";
		public static final String PREPFLASH_API_SERVER = "https://api.prepflash.com:3000/api";
//		public static final String PREPFLASH_API_SERVER = "https://mapi.prepflash.com:3000/api";		
	}
}
