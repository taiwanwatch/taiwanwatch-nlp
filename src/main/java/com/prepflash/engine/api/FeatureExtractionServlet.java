package com.prepflash.engine.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.prepflash.engine.analysis.FeatureExtractor;
import com.prepflash.engine.analysis.SparseVector;
import com.prepflash.engine.util.PrepFlashAPI;

public class FeatureExtractionServlet extends HttpServlet {

	// useKeywords = true: use keywords as inputs
	// useKeywords = false: use contents as inputs
	public static String convertCardsToText( JSONArray json, boolean useKeywords ) {
		String str = "";
		for( int i = 0; i < json.length(); ++i ) {
			JSONObject card = json.getJSONObject(i);
			
			if (useKeywords) {
				JSONArray keys = card.getJSONArray("keyword");
				for( int j = 0; j < keys.length(); ++j ) {
					str += keys.getString(j) + " ";
				}
			} else {
				String ques = card.getString("question");
				String ansr = card.getString("answer");
				String sent = ques.replace("________", ansr );
				str += sent + " ";				
			}
		}
		return str;
	}

	private Map<String, BiFunction<HttpServletRequest, HttpServletResponse, Void>> operatiorMap = new HashMap<String, BiFunction<HttpServletRequest, HttpServletResponse, Void>>();
	private Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	
	public FeatureExtractionServlet() {
		super();
		this.operatiorMap.put("compute_fe_vec", this::doComputeFEVec);
		this.operatiorMap.put("compute_similarity", this::doComputeCosineSimilarity);
	}

	protected final Void doComputeFEVec(HttpServletRequest request, HttpServletResponse response) {
		FeatureExtractor fe = new FeatureExtractor();
		
		String setId = request.getParameter("set_id");
		String text = request.getParameter("text");

		boolean outputHex = false;
		String useFormat = request.getParameter("format");
		if (useFormat != null) {
			outputHex = useFormat.toLowerCase().equals("hex");
		}

		if (setId == null && text == null) {
			return null;
		}

		SparseVector<Double> fv = new SparseVector<Double>();
		if (setId != null) {
			boolean useKeyword = Boolean.parseBoolean(request.getParameter("use_keywords"));
			JSONArray cardSet = PrepFlashAPI.setCards( setId );
			fv = fe.computeFeatureVector( convertCardsToText( cardSet, useKeyword ) );
		} else if (text != null) {
			fv = fe.computeFeatureVector( text );
		}

		try {
			PrintWriter writer = response.getWriter();
			if (outputHex) {
				writer.write(fv.toString((x) -> Integer.toHexString( Float.floatToIntBits( x.floatValue() ) )));
			} else {
				writer.write(fv.toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	protected final Void doComputeCosineSimilarity(HttpServletRequest request, HttpServletResponse response) {
		String fv1Raw = request.getParameter("fv1");
		String fv2Raw = request.getParameter("fv2");

		if (fv1Raw == null || fv2Raw == null) {
			return null;
		}

		boolean useHex = false;
		String useFormat = request.getParameter("format");
		if (useFormat != null) {
			useHex = useFormat.toLowerCase().equals("hex");
		}

		SparseVector<Double> fv1 = new SparseVector<Double>();
		SparseVector<Double> fv2 = new SparseVector<Double>();
		if (!useHex) {
			fv1 = SparseVector.fromJSONString(fv1Raw);
			fv2 = SparseVector.fromJSONString(fv2Raw);
		} else {
			fv1 = SparseVector.fromJSONHexString(fv1Raw);
			fv2 = SparseVector.fromJSONHexString(fv2Raw);			
		}
		
		double sim = FeatureExtractor.cosine_similarity(fv1, fv2);
		Map<String, Double> json = new HashMap<String, Double>();
		json.put("similarity", sim);
		try {
			gson.toJson(json, response.getWriter());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    response.setCharacterEncoding("UTF8"); // this line solves the problem
	    response.setContentType("application/json");
	    PrintWriter writer = response.getWriter();
	    
	    String api_key = request.getParameter("api_key");
	    if (api_key != null && !api_key.equals("5566520forever")) {
	    	writer.write("Wrong API Key. \n");
	    	return;
	    }

	    String op = request.getParameter("operation");
	    BiFunction<HttpServletRequest, HttpServletResponse, Void> func = this.operatiorMap.get(op);
	    if (func != null) {
	    	func.apply(request, response);
	    }
	}
}
