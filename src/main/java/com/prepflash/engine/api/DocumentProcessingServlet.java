package com.prepflash.engine.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.prepflash.engine.documentProcessing.ABBYYClient;
import com.prepflash.engine.documentProcessing.PDFBoxAdapter;
import com.prepflash.engine.generator.SentenceDetector;
import com.abbyy.ocrsdk.ProcessingSettings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@MultipartConfig
public class DocumentProcessingServlet extends HttpServlet 
{
	private Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	private byte[] toByteArray( InputStream is ) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[16384];
		while ((nRead = is.read(data, 0, data.length)) != -1) {
		  buffer.write(data, 0, nRead);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
	
	@Override
	protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String api_key = request.getParameter("api_key");
	    if (api_key != null && !api_key.equals("5566520forever")) {
	    	response.getWriter().write("Wrong API Key. \n");
	    	return;
	    }

	    String outFormat = request.getParameter("out_format");
	    System.out.println("out_format = " + outFormat);
	    if( outFormat == null )
	    	outFormat = "xml";
	    
	    Part filePart = request.getPart("uploadedFile"); 
	    // String fileName = filePart.getSubmittedFileName();
	    InputStream fileIs = filePart.getInputStream();
	    
	    if (fileIs == null) {
	    	return;
	    }
	    
	    byte[] bytes = toByteArray(fileIs);

	    if (bytes.length == 0) {
	    	return;
	    }
	    
	    System.out.println("Upload completed!");
	    	    
	    if( outFormat.equals("txt") )
	    {
		    //ABBYYClient abbyy = new ABBYYClient();
	    	//abbyy.setOutputFormat( ProcessingSettings.OutputFormat.txt);
	    	//response.setHeader("Content-Type", "text/plain; charset=UTF-8");
		    //System.out.println("Set output format: Text");		    
		    //abbyy.process(bytes, response.getOutputStream() );

		    System.out.println("Set output format: Text");		    
			Map<String, Object> json = new HashMap<String, Object>();

			try {
		    	//response.setHeader("Content-Type", "text/plain; charset=UTF-8");
			    response.setHeader("Content-Type", "application/json; charset=UTF-8");
		    	PDFBoxAdapter pdfbox = new PDFBoxAdapter();
		    	// read parse page range
			    String parsePages = request.getParameter("pages");
			    if( parsePages != null ) {
			    	pdfbox.addParsePageRange( parsePages );
			    }
		    	String text = pdfbox.parse(bytes);
		    	text = SentenceDetector.concatenateSentences(text);
				json.put( "data", text );
			} catch ( Exception e ) {
				Map<String, Object> err_obj = new HashMap<String, Object>();
				err_obj.put("errorCode", 620);
				err_obj.put("errorMessage", "Something wrong in PDF parsing. Please check inputs.");
				json.put( "error", err_obj );
			}
			
		    gson.toJson(json, response.getWriter()); // places json string right into the response
	    }
	    else if( outFormat.equals("xml") )
	    {
		    ABBYYClient abbyy = new ABBYYClient();
	    	abbyy.setOutputFormat( ProcessingSettings.OutputFormat.xml);
	    	response.setHeader("Content-Type", "text/xml; charset=UTF-8");
		    System.out.println("Set output format: XML");
		    abbyy.process(bytes, response.getOutputStream() );
	    }
	}
}
