package com.prepflash.engine.api;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prepflash.engine.documentProcessing.PDFBoxAdapter;
import com.prepflash.engine.generator.Card;
import com.prepflash.engine.generator.Sentence;
import com.prepflash.engine.generator.SentenceDetector;
import com.prepflash.engine.generator.SimpleEngine;
import com.prepflash.engine.generator.SimpleEngine.KeywordResult;
import com.prepflash.engine.test.Main;

public class SimpleEngineServlet extends HttpServlet 
{
	static String inputFile = "input.txt";
	
	private static String getStringFromInputStream(InputStream is, Charset encoding) {
		 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is, encoding));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
	}
	
	static String readFile(String filename, Charset encoding) throws IOException, URISyntaxException 
	{
		InputStream in = Main.class.getClassLoader().getResourceAsStream("com/prepflash/engine/test/" + filename);
		return getStringFromInputStream(in, encoding);
	}
	
	static String htmlContent( String url ) 
	{
		try {
			Document doc = Jsoup.connect(url).get();
			Elements nodes = doc.select("p");
			String str = "";
			for( Element node : nodes ) {
				str += node.text() + "\n";
			}
			return str;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	static String pdfUrlToText( String urlStr )
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		URL url = null;
		try { 
			url = new URL(urlStr);
			URLConnection connection = url.openConnection();
			connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.connect();
		  is = connection.getInputStream(); // url.openStream();
		  byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
		  int n;
		  while ( (n = is.read(byteChunk)) > 0 ) {
		    baos.write(byteChunk, 0, n);
		  }
		  is.close();

	      PDFBoxAdapter pdfbox = new PDFBoxAdapter();
	      // read parse page range
	      String text = pdfbox.parse(baos.toByteArray());
	      text = SentenceDetector.concatenateSentences(text);
	      return text;
		}
		catch (IOException e) {
		  System.err.printf ("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
		  e.printStackTrace ();
		}
		return "";
	}

	static String xmlUrlToText( String urlStr )
	{
		try {
			URL url = new URL(urlStr);
			URLConnection connection = url.openConnection();
			connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.connect();

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document doc = db.parse(connection.getInputStream());
			return extractText(doc);
		} catch (Exception e) {
		  e.printStackTrace ();			
		}
		return "";
	}

	static String extractText( Node node )
	{
		String text = "";
		if (node.getNodeName().toLowerCase().equals("text")) {
			text += " " + node.getTextContent();
		}
		if (node.hasChildNodes()) {
			for (int i = 0; i < node.getChildNodes().getLength(); ++i ) {
				text += extractText(node.getChildNodes().item(i));
			}
		}
		return text;
	}


	private SimpleEngine engine = new SimpleEngine();
	private Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	
	@Override
	protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    response.setCharacterEncoding("UTF8"); // this line solves the problem
	    response.setContentType("application/json");
	    PrintWriter writer = response.getWriter();
	    
	    String api_key = request.getParameter("api_key");
	    if (api_key != null && !api_key.equals("5566520forever")) {
	    	writer.write("Wrong API Key. \n");
	    	return;
	    }
	    
	    String url = request.getParameter("url");
	    if (url == null) {
	    	url = "";
	    }
	    
	    String text = request.getParameter("text");
	    if (text == null) {
	    	text = "";
	    }
	    
	    if (text.isEmpty() && (url.startsWith("http://") || url.startsWith("https://"))) {
	    	boolean isPdf = request.getParameter("isPDFUrl") != null 
										 && request.getParameter("isPDFUrl").toLowerCase().equals("true"); 

				boolean isXml = request.getParameter("isXmlUrl") != null 
										 && request.getParameter("isXmlUrl").toLowerCase().equals("true"); 
							 
	    	if (isPdf) {
	    		text = pdfUrlToText(url);
				} else if (isXml) {
	    		text = xmlUrlToText(url);
	    	} else {
	    		text = htmlContent(url);
	    	}
	    }

	    // option
	    SimpleEngine.Option option = new SimpleEngine.Option();
	    
	    // use debug
	    String debug = request.getParameter("debug");
	    if ( debug != null ) {
	    	try {
	    		option.debug = Integer.parseInt(debug); 
	    	} catch(Exception e) {}
	    }

	    // skip sentence pre-processing
    	option.skipSentencePreprocessing = request.getParameter("skipSentencePreprocessing") != null; 

    	// keyword generator options
    	option.keygenUsePrepFlash = request.getParameter("keygenUsePrepFlash") != null 
				                 && request.getParameter("keygenUsePrepFlash").toLowerCase().equals("true"); 
    	
    	option.keygenUseAlchemy = request.getParameter("keygenUseAlchemy") != null 
    						   && request.getParameter("keygenUseAlchemy").toLowerCase().equals("true"); 

    	option.keygenUseSmile = request.getParameter("keygenUseSmile") != null 
    						 && request.getParameter("keygenUseSmile").toLowerCase().equals("true"); 

    	option.keygenUseWiki = request.getParameter("keygenUseWiki") != null 
    						&& request.getParameter("keygenUseWiki").toLowerCase().equals("true"); 

    	option.keygenUseNER = request.getParameter("keygenUseNER") != null 
						   && request.getParameter("keygenUseNER").toLowerCase().equals("true"); 

    	String maxKeyStr = request.getParameter("keygenOptionSmileMaxKeyNum");
    	if ( maxKeyStr != null) {
    		try {
    			int maxKey = Integer.parseInt(maxKeyStr);
    			option.keygenOptionSmileMaxKeyNum = maxKey;
    		} catch (Exception e) {
    		}
    	}

    	boolean runKeywordOnly = request.getParameter("runKeywordOnly") != null 
				              && request.getParameter("runKeywordOnly").toLowerCase().equals("true");

    	// GO!
    	if (!runKeywordOnly) {
			SimpleEngine.Result results = this.engine.createCards(text, url, option); 
	
			Object[] keywords = 
					results.sentences.stream()
							 		 .flatMap((x) -> x.getKeywords().stream())
							 		 .reduce(new HashSet<String>(), 
							 				 (c, x) -> {
							 					c.add(x.getText());
							 					return c;
							 				 }, (c1, c2) -> {
							 					c1.addAll(c2);
												return c1;
							 				 })
							 		 .toArray();
			Map<String, Object> json = new HashMap<String, Object>();
			json.put( "data", results.cards );
			json.put( "keyword", keywords);
		    gson.toJson(json, writer); // places json string right into the response
    	} else {
    		List<KeywordResult> result = this.engine.createKeywords(text, url, option);
		    gson.toJson(result, writer); // places json string right into the response
    	}
	} 
	
	@Override
	protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet( request, response );
	}
}
