package com.prepflash.engine.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.prepflash.engine.common.Configuration;

public class PrepFlashAPI {

	private static String API_USERNAME = Configuration.SETTINGS_PCKG.PREPFLASH_API_USERNAME;
	private static String API_PASSWROD = Configuration.SETTINGS_PCKG.PREPFLASH_API_PASSWROD;
	private static String API_SERVER = Configuration.SETTINGS_PCKG.PREPFLASH_API_SERVER;
	private static String API_KEY = null;
		
	@SuppressWarnings("unchecked")
	private static <T> T processHttpRequest( String url_str, JSONObject post_body, String httpMethod, Class<T> clazz ) {
		try {
			URL url = new URL( API_SERVER + url_str );
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if( httpMethod == "POST" )
				conn.setDoOutput(true);

			conn.setRequestMethod(httpMethod);
				        
	        conn.addRequestProperty("Content-type", "application/json");
	        if( API_KEY != null ) {
	        	conn.addRequestProperty("Authorization", API_KEY);
	        }

	        if( httpMethod == "POST" ) {
		        String json_str = post_body.toString();
		        
		        DataOutputStream ostream = new DataOutputStream(conn.getOutputStream());
		        ostream.write( json_str.getBytes() );
		        ostream.close();
	        }

	        //System.out.println( "API_REQUEST = " + url );
	        
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String line = null;
			String output = "";
			while ((line = br.readLine()) != null) {
				output += line;
			}			
			conn.disconnect();
			
			//System.out.println( "API_RESPONSE = " + output );
			
			JSONObject json = new JSONObject(output);
			T data = null;
			if( clazz.equals( JSONObject.class ) ) {
				data = (T) json.getJSONObject("data");
			} else if( clazz.equals( JSONArray.class ) ) {
				data = (T) json.getJSONArray("data");
			}
			
			if( data != null ) {
				return data;
			} else {
				JSONObject err = json.getJSONObject("error");
				if( err != null ) {
					System.out.println( err );
				} else {
					System.out.println( "Unknown API results" );
				}
				return null;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {		 
			e.printStackTrace();
		} 
		return null;
	}
	
	private static <T> T processHttpRequestGet( String url_str, Class<T> clazz ) {
		return processHttpRequest( url_str, null, "GET", clazz );
	}
	
	private static void Login() {
		JSONObject json = new JSONObject();
		json.put("username", API_USERNAME );
		json.put("password", API_PASSWROD );
		
		json = processHttpRequest( "/users/login/", json, "POST", JSONObject.class );
		if( json != null ) {
			API_KEY = json.getString("id");
		}
	}
	
	public static JSONArray setCards( String set_id ) {
		if( API_KEY == null )
			Login();
		return processHttpRequestGet("/sets/" + set_id + "/cards", JSONArray.class );
	}
}
