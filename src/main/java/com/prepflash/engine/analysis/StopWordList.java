package com.prepflash.engine.analysis;

import java.util.List;

public interface StopWordList {
	List<String> getList();
}
