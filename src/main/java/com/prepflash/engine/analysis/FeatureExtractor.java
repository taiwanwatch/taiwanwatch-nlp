package com.prepflash.engine.analysis;

import java.util.List;
import java.util.stream.Collectors;

import com.prepflash.engine.generator.StanfordSentenceAnnotator;

public class FeatureExtractor {

    private StanfordSentenceAnnotator lemma = new StanfordSentenceAnnotator();
    private StopWordChecker swchk = new StopWordChecker( new EnglishDefaultStopWord(),
    													 new EnglishLongStopWord(),
    													 new GoogleHistoryStopWord(), 
    													 new MySQLStopWord() );

	public SparseVector<Integer> computeCountVector( String text ) {
	    List<String> lem = lemma.lemmatize(text);
	    
	    // apply stop-word filter
	    lem = lem.stream()
	    		 .parallel()
	    		 .filter((x) -> !swchk.isStopWord(x) )
	    		 .collect(Collectors.toList());
	    
	    return this.computeCountVector( lem );
	}
	
	public SparseVector<Integer> computeCountVector( List<String> lemmatized_text ) {
		SparseVector<Integer> tf_vec = new SparseVector<Integer>();
		for( String word : lemmatized_text ) {
			int hash = word.hashCode();
			Integer cnt = tf_vec.get( hash );
			if( cnt == null )
				tf_vec.put(hash, 1);
			else
				tf_vec.put(hash, cnt+1);
		}
		return tf_vec;
	}
	
	public SparseVector<Double> computeFeatureVector( String text ) {
		SparseVector<Integer> cnt_v = this.computeCountVector( text );
		SparseVector<Double>  norm  = cnt_v.normalize();
		return norm;
	}
		
	static public <T extends Number> double cosine_similarity( SparseVector<T> fv_i, SparseVector<T> fv_j ) {
		double up     = fv_i.dot( fv_j ).doubleValue();
		double down   = fv_i.length() * fv_j.length();
		double cos_th = up / down;
		return cos_th;
	}
}
