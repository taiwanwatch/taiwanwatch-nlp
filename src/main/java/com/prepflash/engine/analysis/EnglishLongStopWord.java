package com.prepflash.engine.analysis;

import java.util.List;

public class EnglishLongStopWord implements StopWordList {
	private static List<String> list = null;
	
	public EnglishLongStopWord() {
		if( EnglishLongStopWord.list == null ) {
			String list_text = "stop_word_en_long.txt";
			String fname = EnglishLongStopWord.class.getClassLoader().getResource("com/prepflash/engine/analysis/" + list_text).getFile();
			TextFileStopWord o = new TextFileStopWord( fname );
			EnglishLongStopWord.list = o.getList();
		}
	}
	
	@Override
	public List<String> getList() {
		return EnglishLongStopWord.list;
	}

}
