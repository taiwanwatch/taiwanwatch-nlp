package com.prepflash.engine.analysis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.json.JSONObject;

import java.lang.Class;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.stanford.nlp.util.Sets;

public class SparseVector<T extends Number> {

	private HashMap<Integer, T> data = new HashMap<Integer, T>();
	
	private static SparseVector<Double> implFromJSONString( String jsonString, boolean useHexString ) {
		JSONObject json = new JSONObject(jsonString);
		SparseVector<Double> fv = new SparseVector<Double>();
	    Iterator<String> keysItr = json.keys();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        String value = (String) json.get(key);
	        if (!useHexString) {
	        	fv.put(Integer.parseInt(key), Double.parseDouble(value));
	        } else {
	        	Integer id = Integer.parseInt(key, 16);
	        	Float val = Float.intBitsToFloat( Integer.parseInt(value, 16) );
	        	fv.put(id, val.doubleValue());
	        }
	    }
		return fv;
	}

	public static SparseVector<Double> fromJSONString( String jsonString ) {
		return implFromJSONString(jsonString, false);
	}

	public static SparseVector<Double> fromJSONHexString( String jsonString ) {
		return implFromJSONString(jsonString, true);
	}

	public void put( Integer id, T value ) {
		if( value != null ) {
			data.put(id, value);
		}
	}
	
	public T get( Integer id ) {
		return data.get( id );
	}
	
	public void remove( Integer id ) {
		data.remove(id);
	}
	
	public void clear( Integer id ) {
		data.clear();
	}
	
	public Set<Integer> getNonZeroIndex() {
		return data.keySet();
	}
	
	public Map<Integer, T> getNonZeroData() {
		return data;
	}
	
	public int numberOfNonZeroEntry() {
		return this.data.size();
	}
	
	@SuppressWarnings("unchecked")
	public T dot( SparseVector<T> v ) {
		Set<Integer> idx_i = this.getNonZeroIndex();
		Set<Integer> idx_j = v.getNonZeroIndex();
		Set<Integer> idx_intsct = Sets.intersection(idx_i, idx_j);
		double sum = 0;
		for( Integer id : idx_intsct ) {
			sum += this.get(id).doubleValue() * v.get(id).doubleValue();
		}
		return (T)Double.valueOf(sum);
	}
	
	public double length() {
		double len = 0;
		for( Integer id : this.getNonZeroIndex() ) {
			double val = this.get(id).doubleValue();
			len += val*val;
		}
		len = Math.sqrt(len);
		return len;
	}

	public SparseVector<Double> normalize() {
		double len = this.length();
		SparseVector<Double> v = new SparseVector<Double>();
		for( Integer id : this.getNonZeroIndex() ) {
			double val = this.get(id).doubleValue();
			v.put(id, val / len);
		}
		return v;
	}
	
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		return gson.toJson(this.data); 
	}
	
	public String toString( Function<T, String> value_to_hex_func ) {
		HashMap<String, String> hex_map = new HashMap<String, String>();
		for( Integer id : this.getNonZeroIndex() ) {
			T val = this.get(id);
			hex_map.put( Integer.toHexString(id), value_to_hex_func.apply(val) );
		}
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		return gson.toJson( hex_map );
	}

}
