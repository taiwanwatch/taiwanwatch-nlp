package com.prepflash.engine.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextFileStopWord implements StopWordList {

	private File file;
	private ArrayList<String> list = new ArrayList<String>();
	
	public TextFileStopWord( String filename ) {
		this( new File(filename) );
	}
	
	public TextFileStopWord( File f ) {
		this.file = f;
		try {
			this.readStringsFromFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<String> getList() {
		return this.list;
	}

	private void readStringsFromFile() throws IOException {
		BufferedReader reader = null;
	    String         line   = null;

	    try {
	    	 reader = new BufferedReader(new FileReader (this.file));
	    	 this.list.clear();
	         while((line = reader.readLine()) != null) {
	        	 line = line.toLowerCase().trim();
	        	 if( !line.isEmpty() )
	        		 this.list.add( line );
	         }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if( reader != null ) {
				reader.close();
			}
		}

	}
}
