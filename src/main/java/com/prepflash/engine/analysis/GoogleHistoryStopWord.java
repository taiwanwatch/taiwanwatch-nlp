package com.prepflash.engine.analysis;

import java.util.List;

public class GoogleHistoryStopWord implements StopWordList {
	private static List<String> list = null;
	
	public GoogleHistoryStopWord() {
		if( GoogleHistoryStopWord.list == null ) {
			String list_text = "stop_word_en_google_history.txt";
			String fname = GoogleHistoryStopWord.class.getClassLoader().getResource("com/prepflash/engine/analysis/" + list_text).getFile();
			TextFileStopWord o = new TextFileStopWord( fname );
			GoogleHistoryStopWord.list = o.getList();
		}
	}
	
	@Override
	public List<String> getList() {
		return GoogleHistoryStopWord.list;
	}

}
