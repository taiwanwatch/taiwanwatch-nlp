package com.prepflash.engine.analysis;

import java.util.List;

public class MySQLStopWord implements StopWordList {
	private static List<String> list = null;
	
	public MySQLStopWord() {
		if( MySQLStopWord.list == null ) {
			String list_text = "stop_word_mysql.txt";
			String fname = MySQLStopWord.class.getClassLoader().getResource("com/prepflash/engine/analysis/" + list_text).getFile();
			TextFileStopWord o = new TextFileStopWord( fname );
			MySQLStopWord.list = o.getList();
		}
	}
	
	@Override
	public List<String> getList() {
		return MySQLStopWord.list;
	}

}
