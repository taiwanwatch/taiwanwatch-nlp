package com.prepflash.engine.analysis;

import java.util.List;

public class EnglishDefaultStopWord implements StopWordList {

	private static List<String> list = null;
	
	public EnglishDefaultStopWord() {
		if( EnglishDefaultStopWord.list == null ) {
			String list_text = "stop_word_en_default.txt";
			String fname = EnglishDefaultStopWord.class.getClassLoader().getResource("com/prepflash/engine/analysis/" + list_text).getFile();
			TextFileStopWord o = new TextFileStopWord( fname );
			EnglishDefaultStopWord.list = o.getList();
		}
	}
	
	@Override
	public List<String> getList() {
		return EnglishDefaultStopWord.list;
	}

}
