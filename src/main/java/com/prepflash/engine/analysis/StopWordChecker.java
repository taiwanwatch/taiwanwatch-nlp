package com.prepflash.engine.analysis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class StopWordChecker {
	private ArrayList<StopWordList> lists = new ArrayList<StopWordList>();
	private HashSet<String> words = new HashSet<String>();
	
	public StopWordChecker( StopWordList... l ) {
		this.lists.addAll( Arrays.asList(l) );
		
		for( StopWordList s : this.lists ) {
			words.addAll( s.getList() );
		}
	}
	
	public boolean isStopWord( String s ) {
		return this.words.contains( s.toLowerCase() );
	}
}
