package com.prepflash.engine.documentProcessing;

import java.io.*;
import java.util.ArrayList;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

public class PDFBoxAdapter 
{
	private class PageRange 
	{
		public int start = -1;
		public int end = -1;
		
		PageRange( int start, int end ) {
			this.start = start;
			this.end = end;
		}
	}
	
	private ArrayList<PageRange> page_ranges = new ArrayList<PageRange>();
	
	public void addParsePageRange( int start, int end )
	{
		this.page_ranges.add(new PageRange( start, end ));
	}

	public void addParsePage( int page )
	{
		this.page_ranges.add(new PageRange( page, page ));
	}

	// example of page_str = 1, 5, 12-23, 45-23, 5 
	public void addParsePageRange( String page_str )
	{
		String [] pgs_str = page_str.split(",");
		for( String s : pgs_str ) {
			s = s.trim();
			if( s.contains("-") ) { // a range
				String [] pgrng = s.split("-");
				if( pgrng.length >= 2 ) { // a defined range
					try {
						int beg = Integer.parseInt( pgrng[0].trim() );
						int end = Integer.parseInt( pgrng[1].trim() );
						this.addParsePageRange(beg, end);
					} catch ( NumberFormatException e ) {
						// do nothing
					}
				} else if( pgrng.length == 1 ) { // a range from given page to last page
					try {
						int beg = Integer.parseInt( pgrng[0].trim() );
						int end = Integer.MAX_VALUE;
						this.addParsePageRange(beg, end);
					} catch ( NumberFormatException e ) {
						// do nothing
					}
				}
			} else { // a page
				try {
					int pg = Integer.parseInt(s);
					this.addParsePage(pg);
				} catch ( NumberFormatException e ) {
					// do nothing
				}
			}
		}		
	}
	
	public String parse( byte[] bytes )
	{
        try {
            PDDocument pdDoc = PDDocument.load( new ByteArrayInputStream( bytes ));
            PDFTextStripper pdfStripper = new PDFTextStripper("UTF-8");
            String parsedText = "";
            if( this.page_ranges.size() == 0 ) {
            	parsedText = pdfStripper.getText(pdDoc);
            } else {
            	for( PageRange pr : this.page_ranges ) {
            		if( pr.start >= 1 && pr.end >= pr.start ) {
            			pdfStripper.setStartPage( pr.start );
            			pdfStripper.setEndPage( pr.end );
            			parsedText += pdfStripper.getText(pdDoc);
            		}
            	}
            }
            pdDoc.close();
            return parsedText;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } 
	}
	
	public String parse( InputStream ins )
	{
        try {
        	byte[] bytes = toByteArray( ins );
        	return parse( bytes );
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } 
	}
	
	private byte[] toByteArray( InputStream is ) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[16384];
		while ((nRead = is.read(data, 0, data.length)) != -1) {
		  buffer.write(data, 0, nRead);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
}
