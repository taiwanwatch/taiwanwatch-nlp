package com.prepflash.engine.documentProcessing;

import java.io.OutputStream;

import com.abbyy.ocrsdk.Client;
import com.abbyy.ocrsdk.ProcessingSettings;
import com.abbyy.ocrsdk.Task;

public class ABBYYClient {
	
	private Client restClient;
	private ProcessingSettings.OutputFormat outputFormat = ProcessingSettings.OutputFormat.xml;

	public ABBYYClient() {
		ClientSettings.setupProxy();
		
		restClient = new Client();
		// replace with 'https://cloud.ocrsdk.com' to enable secure connection
		restClient.serverUrl = "http://cloud.ocrsdk.com";
		restClient.applicationId = ClientSettings.APPLICATION_ID;
		restClient.password = ClientSettings.PASSWORD;
	}
	
	public void setOutputFormat( ProcessingSettings.OutputFormat format )
	{
		this.outputFormat = format;
	}
	
	public void process( byte[] data, OutputStream out ) {
		if (data == null || data.length == 0) {
			return;
		}
		
		ProcessingSettings settings = new ProcessingSettings();
		settings.setLanguage("English");
		settings.setOutputFormat(this.outputFormat);

		try {
			Task task = restClient.processImage(data, settings);
			this.waitAndDownloadResult(task, out);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/** 
	 * Wait until task processing finishes
	 */
	private Task waitForCompletion(Task task) throws Exception {
		// Note: it's recommended that your application waits
		// at least 2 seconds before making the first getTaskStatus request
		// and also between such requests for the same task.
		// Making requests more often will not improve your application performance.
		// Note: if your application queues several files and waits for them
		// it's recommended that you use listFinishedTasks instead (which is described
		// at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
		while (task.isTaskActive()) {

			Thread.sleep(5000);
			System.out.println("Waiting..");
			task = restClient.getTaskStatus(task.Id);
		}
		return task;
	}

	/**
	 * Wait until task processing finishes and download result.
	 */
	private void waitAndDownloadResult(Task task, OutputStream out)
			throws Exception {
		task = waitForCompletion(task);

		if (task.Status == Task.TaskStatus.Completed) {
			System.out.println("Downloading..");
			restClient.downloadResult(task, out);
			System.out.println("Ready");
		} else if (task.Status == Task.TaskStatus.NotEnoughCredits) {
			System.out.println("Not enough credits to process document. "
					+ "Please add more pages to your application's account.");
		} else {
			System.out.println("Task failed");
		}

	}

}
