package com.prepflash.engine.documentProcessing;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.prepflash.engine.xsd.abbyy.*;

public class MainContentExtractor {
	private Unmarshaller um;
	
	public MainContentExtractor() {
		try {
			JAXBContext jc = JAXBContext.newInstance(com.prepflash.engine.xsd.abbyy.Document.class);
			um = jc.createUnmarshaller();
		} catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	public void process( InputStream is ) {
		try {
			Document root = (Document) um.unmarshal(is);
			
//			double avgCharSize = calculateMeanCharSize( root );
//			System.out.println("Avg Char Size = " + avgCharSize);

			double avgLineH = this.calculateMeanLineHeight( root );
			System.out.println("Avg Line Height = " + avgLineH);

			String outTxt = this.extractText(root, avgLineH);
			System.out.println(outTxt);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
		
	private double calculateMeanCharSize( Document root ) {
		return
		root.getPage()
			.stream()
			.flatMap( (p) -> p.getBlock().stream() )
			.filter((b) -> { return b.getBlockType().equals("Text"); } )
			.flatMap( (b) -> b.getText().stream() )
			.flatMap( (t) -> t.getPar().stream() )
			.flatMap( (pr) -> pr.getLine().stream() )
			.flatMap( (l) -> l.getFormatting().stream() )
			.flatMap( (f) -> f.getContent().stream() )
			.parallel()
			.filter((x) -> {
				if (x instanceof JAXBElement) {
					String tag = ((JAXBElement)x).getName().getLocalPart();
					return tag.equals( "charParams" );
				} else {
					return false;
				}
			})
			.map( (x) -> ((JAXBElement<CharParamsType>)x).getValue() )
			.mapToDouble((x) -> {
				CharParamsType c = (CharParamsType)x;
				double w = c.getR().doubleValue() - c.getL().doubleValue();
				double h = c.getB().doubleValue() - c.getT().doubleValue();
				return w*h;
			})
			.average()
			.getAsDouble();
	}

	private double calculateMeanLineHeight( Document root ) {
		return
		root.getPage()
			.stream()
			.flatMap( (p) -> p.getBlock().stream() )
			.filter((b) -> { return b.getBlockType().equals("Text"); } )
			.flatMap( (b) -> b.getText().stream() )
			.flatMap( (t) -> t.getPar().stream() )
			.flatMap( (pr) -> pr.getLine().stream() )
			.parallel()
			.mapToDouble((x) -> x.getB().doubleValue() - x.getT().doubleValue())
			.average()
			.getAsDouble();
	}

	private String extractText( Document doc, double avgLineH ) {
		return doc.getPage()
		 .stream()
		 .flatMap((x)-> x.getBlock().stream())
		 .filter((b) -> { return b.getBlockType().equals("Text"); } )
		 .flatMap( (x) -> x.getText().stream() )
		 .flatMap( (t) -> t.getPar().stream() )
		 .filter((par) -> {
				boolean ok = true;
				ok &= this.checkParLineNumber( par );
//				ok &= this.checkParCharSize( par, avgCharSize );
				ok &= this.checkParLineHeight(par, avgLineH);
				return ok;
		 })
		 .map( (par) -> {
			 return
			 par.getLine()
				.stream()
				.flatMap( (l) -> l.getFormatting().stream() )
				.flatMap( (f) -> f.getContent().stream() )
				.filter((n) -> {
					if (n instanceof JAXBElement) {
						String tag = ((JAXBElement)n).getName().getLocalPart();
						return tag.equals( "charParams" );
					 } else {
						return false;
					 }
				 })
				 .map( (n) -> ((JAXBElement<CharParamsType>)n).getValue() )
				 .flatMap( (n) -> n.getContent().stream() )
				 .filter((c) -> c instanceof String)
				 .map( (c) -> (String)c )
				 .reduce( "", String::concat );
		 })
		 .reduce( "", (str, parStr) -> str + parStr + "\n\n\n");
	}
	
	private boolean checkParLineNumber( ParagraphType par ) {
		return par.getLine().size() >= 3;
	}
	
	private boolean checkParCharSize( ParagraphType par, double avgCharSize ) {
		double rmse = par.getLine()
				.stream()
				.flatMap( (l) -> l.getFormatting().stream() )
				.flatMap( (f) -> f.getContent().stream() )
				.parallel()
				.filter((x) -> {
					if (x instanceof JAXBElement) {
						String tag = ((JAXBElement)x).getName().getLocalPart();
						return tag.equals( "charParams" );
					} else {
						return false;
					}
				})
				.map( (x) -> ((JAXBElement<CharParamsType>)x).getValue() )
				.mapToDouble((x) -> {
					CharParamsType c = (CharParamsType)x;
					double w = c.getR().doubleValue() - c.getL().doubleValue();
					double h = c.getB().doubleValue() - c.getT().doubleValue();
					double a = w*h;
					double m = (a - avgCharSize) * (a - avgCharSize);
					return m;
				})
				.average()
				.getAsDouble();

		rmse = Math.sqrt(rmse);
		return rmse < 200.0;
	}
	
	private boolean checkParLineHeight( ParagraphType par, double avgLineH ) {
		double rmse = par.getLine()
				.stream()
				.parallel()
				.mapToDouble((x) -> {
					double h = x.getB().doubleValue() - x.getT().doubleValue();
					double m = (h - avgLineH) * (h - avgLineH);
					return m;
				})
				.average()
				.getAsDouble();

		rmse = Math.sqrt(rmse);
		System.out.println(rmse);
		return rmse < 0.5*avgLineH;
	}

}
