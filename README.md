# **Compute FE (feature extraction) vector** #

## endpoint: {ENGINE_ADDR}/featureExtraction ##

| key | value | note
|---|----|-----|
| api_key | 5566520forever | |
| operation | compute_fe_vec | |
| format | hex | (optional) default output is plain text. Use "hex" to output 4 bytes integer / float pairs to preserve precisions |

## 1) FE from plain text: ##

| key | value | note
|---|----|-----|
| text | what ever text as inputs | |

## 2) FE from card sets (Note that PrepFlash server should be configured in Configuration.java): ##

| key | value | note
|---|----|-----|
| set_id | `eb9d4e80-754f-11e6-aa58-c7d40f29a01f` | |
| use_keywords | true | (optional) default is to use raw contents of all cards. Set this flag to use only keywords as inputs |

## output example: ##
```
{
  "51": 0.02779923608748833,
  "2154": 0.02779923608748833,
  "66532": 0.02779923608748833,
  "112682": 0.05559847217497666,
  "1515396": 0.083397708262465
}
```

## output example (Hex): ##
```
{
  "ac02d029": "3ce3bb39",
  "2cb8b0c5": "3ce3bb39",
  "f41fb3c2": "3ce3bb39",
  "3b953587": "3d63bb39"
}
```

# **Compute similarity of two FE vectors** #

## endpoint: {ENGINE_ADDR}/featureExtraction ##

| key | value | note
|---|----|-----|
| api_key | 5566520forever | |
| operation | compute_similarity | |
| format | hex | (optional) default input is plain text. Use "hex" to use 4 bytes integer / float pairs as inputs |
| fv1 | `{"51": 0.0289, "123": 0298}` or in hex format | FE vector 1 |
| fv2 | `{"51": 0.0289, "123": 0298}` or in hex format | FE vector 2 |

## output example (-1 to 1): ##
```
{
  "similarity": 0.9999999999999999
}
```
